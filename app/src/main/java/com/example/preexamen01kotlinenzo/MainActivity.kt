package com.example.preexamen01kotlinenzo

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

// Proyecto terminado Enzo Maciel
class MainActivity : AppCompatActivity() {
    private var btnEntrar: Button? = null
    private var btnSalir: Button? = null
    private var txtNombreTrabajador: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEntrar!!.setOnClickListener { ingresar() }
        btnSalir!!.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador)
    }

    private fun ingresar() {
        val strUsuario: String
        strUsuario = resources.getString(R.string.nombre)
        if (strUsuario == txtNombreTrabajador!!.text.toString()) {
            // Crear un Bundle para enviar información
            val bundle = Bundle()
            bundle.putString("nombre", txtNombreTrabajador!!.text.toString())

            // Crear un intent para llamar a otra actividad
            val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando o no una respuesta
            startActivity(intent)
        } else {
            Toast.makeText(applicationContext, "Compruebe el nombre", Toast.LENGTH_SHORT).show()
        }
    }

    private fun salir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo Nómina")
        confirmar.setMessage("Salir de la app")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialogInterface, i -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialogInterface, i ->
            // Cancelar, no se realiza ninguna acción
        }
        confirmar.show()
    }
}