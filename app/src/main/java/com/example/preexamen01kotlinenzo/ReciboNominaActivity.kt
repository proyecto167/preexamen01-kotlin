package com.example.preexamen01kotlinenzo

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {
    private var lblNombreUsuario: TextView? = null
    private var txtNumRecibo: EditText? = null
    private var txtNombre: EditText? = null
    private var txtHorasNormal: EditText? = null
    private var txtHorasExtras: EditText? = null
    private var rdbPuesto1: RadioButton? = null
    private var rdbPuesto2: RadioButton? = null
    private var rdbPuesto3: RadioButton? = null
    private var txtSubtotal: EditText? = null
    private var txtImpuesto: EditText? = null
    private var txtTotal: EditText? = null
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnRegresar: Button? = null

    // Declarar objeto
    var recibo = ReciboNomina()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibonomina)
        iniciarComponentes()
        val datos = intent.extras
        if (datos != null) {
            val usuario = datos.getString("nombre")
            if (usuario != null) {
                lblNombreUsuario!!.text = usuario
            }
        }
        btnCalcular!!.setOnClickListener { metodoCalcular() }
        btnLimpiar!!.setOnClickListener { metodoLimpiar() }
        btnRegresar!!.setOnClickListener { metodoRegresar() }
    }

    private fun iniciarComponentes() {
        // Asignar los atributos privados a los elementos del diseño
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasNormal = findViewById(R.id.txtHorasNormal)
        txtHorasExtras = findViewById(R.id.txtHorasExtras)
        rdbPuesto1 = findViewById(R.id.rdbPuesto1)
        rdbPuesto2 = findViewById(R.id.rdbPuesto2)
        rdbPuesto3 = findViewById(R.id.rdbPuesto3)
        txtSubtotal = findViewById(R.id.txtSubtotal)
        txtImpuesto = findViewById(R.id.txtImpuesto)
        txtTotal = findViewById(R.id.txtTotal)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        lblNombreUsuario = findViewById(R.id.lblNombreUsuario)

        // Inicializar la calculadora
        recibo = ReciboNomina()
    }

    private fun metodoCalcular() {
        // Puesto
        var puesto = 0
        var horasNormales = 0f
        var horasExtras = 0f
        var subtotal = 0f
        var impuesto = 0f
        var total = 0f
        if (rdbPuesto1!!.isChecked) {
            puesto = 1
        } else if (rdbPuesto2!!.isChecked) {
            puesto = 2
        } else if (rdbPuesto3!!.isChecked) {
            puesto = 3
        }
        if (txtNumRecibo!!.text.toString().isEmpty()) {
            Toast.makeText(this, "Ingrese su Número de Recibo", Toast.LENGTH_SHORT).show()
        } else if (txtNombre!!.text.toString().isEmpty()) {
            Toast.makeText(this, "Ingrese su Nombre", Toast.LENGTH_SHORT).show()
        } else if (txtHorasNormal!!.text.toString().isEmpty() || txtHorasExtras!!.text.toString()
                .isEmpty()
        ) {
            if (txtHorasNormal!!.text.toString().isEmpty()) {
                Toast.makeText(this, "Ingrese la cantidad de Horas Trabajadas", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(
                    this,
                    "Ingrese la cantidad de Horas Extras Trabajadas",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        // Horas trabajadas
        if (!txtHorasNormal!!.text.toString().isEmpty()) {
            horasNormales = txtHorasNormal!!.text.toString().toFloat()
        }
        if (!txtHorasExtras!!.text.toString().isEmpty()) {
            horasExtras = txtHorasExtras!!.text.toString().toFloat()
        }


        // setear valores a nomina
        recibo.puesto = puesto
        recibo.horasTrabNormal = horasNormales
        recibo.horasTrabExtras = horasExtras

        // Llamar a los métodos
        if (!txtHorasNormal!!.text.toString().isEmpty() && !txtHorasExtras!!.text.toString()
                .isEmpty() && puesto != 0
        ) {
            subtotal = recibo.CalcularSubtotal()
            impuesto = recibo.CalcularImpuesto(subtotal)
            total = recibo.CalcularTotal(subtotal, impuesto)
            txtSubtotal!!.setText(subtotal.toString())
            txtImpuesto!!.setText(impuesto.toString())
            txtTotal!!.setText(total.toString())
        }
    }

    private fun metodoLimpiar() {
        txtNumRecibo!!.setText("")
        txtNombre!!.setText("")
        txtHorasNormal!!.setText("")
        txtHorasExtras!!.setText("")
        txtSubtotal!!.setText("")
        txtImpuesto!!.setText("")
        txtTotal!!.setText("")

        // Deseleccionar los RadioButtons
        rdbPuesto1!!.isChecked = true
        rdbPuesto2!!.isChecked = false
        rdbPuesto3!!.isChecked = false
    }

    private fun metodoRegresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Cálculo de Pago de Nómina")
        confirmar.setMessage("Regresar al inicio de sesión")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialogInterface, which -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialogInterface, which ->
            // No hacer nada
        }
        confirmar.show()
    }
}
