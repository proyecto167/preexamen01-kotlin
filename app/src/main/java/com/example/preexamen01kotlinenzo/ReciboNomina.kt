package com.example.preexamen01kotlinenzo

class ReciboNomina
{
    var numRecibo = 0
    var nombre: String? = null
    var horasTrabNormal = 0f
    var horasTrabExtras = 0f
    var puesto = 0
    var impuestoPorc = 0f

    // Métodos
    fun CalcularSubtotal(): Float {
        var subtotal = 0f
        var pagoBase = 200f
        return when (puesto) {
            1 -> {
                // Caso Auxiliar
                pagoBase = (200 + 200 * 20 / 100).toFloat()
                subtotal = pagoBase * horasTrabNormal + pagoBase * horasTrabExtras * 2
                subtotal
            }

            2 -> {
                // Caso Albañil
                pagoBase = (200 + 200 * 50 / 100).toFloat()
                subtotal = pagoBase * horasTrabNormal + pagoBase * horasTrabExtras * 2
                subtotal
            }

            3 -> {
                // Caso Ing. Obra
                pagoBase = (200 + 200).toFloat()
                subtotal = pagoBase * horasTrabNormal + pagoBase * horasTrabExtras * 2
                subtotal
            }

            else -> subtotal
        }
    }

    fun CalcularImpuesto(subtotal: Float): Float {
        val impuesto: Float
        impuesto = subtotal * 16 / 100
        return impuesto
    }

    fun CalcularTotal(subtotal: Float, impuesto: Float): Float {
        val total: Float
        total = subtotal - impuesto
        return total
    }
}
